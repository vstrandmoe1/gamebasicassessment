package osm.gamebasicassesment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import osm.gamebasicassesment.adapters.MatchHistoryAdapter;
import osm.gamebasicassesment.adapters.PouleTableAdapter;
import osm.gamebasicassesment.models.Team;
import osm.gamebasicassesment.utils.GameEngine;
import osm.gamebasicassesment.utils.TournamentPoule;

public class MainActivity extends AppCompatActivity {


    Button butPlayMatch;
    Button butResetTournament;
    Button butToStandings;

    static TextView tvNextMatchTeamNames, tvNextMatchResult;
    static ImageView ivFlagHome, ivFlagAway;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvNextMatchTeamNames = (TextView) findViewById(R.id.tv_next_match_names);
        tvNextMatchResult = (TextView) findViewById(R.id.tv_next_match_results);
        butResetTournament = (Button) findViewById(R.id.but_resetTournament);
        butToStandings = (Button) findViewById(R.id.but_standings);

        ivFlagHome = (ImageView) findViewById(R.id.iv_flagHome);
        ivFlagAway = (ImageView) findViewById(R.id.iv_flagAway);

        TournamentPoule.createNewTournament();

        butPlayMatch = (Button) findViewById(R.id.but_playMatch);
        butPlayMatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Team teamHome = null;
                Team teamAway = null;

                //logic for playing a set amount of games.
                if(TournamentPoule.matchNumber <=6) {
                    switch (TournamentPoule.matchNumber) {
                        case 1:
                            teamHome = TournamentPoule.matchSchedule.get(0);
                            teamAway = TournamentPoule.matchSchedule.get(1);
                            break;
                        case 2:
                            teamHome = TournamentPoule.matchSchedule.get(2);
                            teamAway = TournamentPoule.matchSchedule.get(3);
                            break;
                        case 3:
                            teamHome = TournamentPoule.matchSchedule.get(3);
                            teamAway = TournamentPoule.matchSchedule.get(0);
                            break;
                        case 4:
                            teamHome = TournamentPoule.matchSchedule.get(1);
                            teamAway = TournamentPoule.matchSchedule.get(2);
                            break;
                        case 5:
                            teamHome = TournamentPoule.matchSchedule.get(1);
                            teamAway = TournamentPoule.matchSchedule.get(3);
                            break;
                        case 6:
                            teamHome = TournamentPoule.matchSchedule.get(0);
                            teamAway = TournamentPoule.matchSchedule.get(2);
                            break;
                    }
                    tvNextMatchTeamNames.setText(teamHome.getStrName() + " - " + teamAway.getStrName());
                    ivFlagHome.setVisibility(View.VISIBLE);
                    ivFlagAway.setVisibility(View.VISIBLE);
                    ivFlagHome.setImageResource(teamHome.getResourceID());
                    ivFlagAway.setImageResource(teamAway.getResourceID());


                    //Go to game
                    TournamentPoule.playNextGame();


                    tvNextMatchResult.setText(GameEngine.getCurrentMatchResultsInString());

                    if (StandingsActivity.standingsAdapter != null) {
                        StandingsActivity.standingsAdapter.notifyDataSetChanged();
                    }

                    if (StandingsActivity.historyAdapter != null) {
                        StandingsActivity.historyAdapter.notifyDataSetChanged();
                    }
                }else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage("End of Poule games. Want to see score or start again?");
                    builder.setCancelable(false);
                    builder.setPositiveButton(
                            "Show table standings",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    //end of poule rounds. highlight teams going through toknockout stage by hckign position
                                    //one last time

                                    if(StandingsActivity.standingsAdapter!=null) {
                                        StandingsActivity.standingsAdapter.highlightTeamsGoingToKnockout();
                                    }
                                    Intent intent = new Intent(MainActivity.this, StandingsActivity.class);
                                    startActivity(intent);

                                    dialog.cancel();

                                }
                            });

                    builder.setNegativeButton(
                            "Reset tournament",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    butResetTournament.callOnClick();
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = builder.create();
                    alert11.show();
                }
            }
        });

        //click listener for reseting the stats of the tournament
        butResetTournament.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                TournamentPoule.createNewTournament();
                tvNextMatchTeamNames.setText("");
                tvNextMatchResult.setText("");
                ivFlagHome.setVisibility(View.INVISIBLE);
                ivFlagAway.setVisibility(View.INVISIBLE);
                if(StandingsActivity.standingsAdapter !=null) {
                    PouleTableAdapter.endOfPoule = false;
                    StandingsActivity.standingsAdapter.removeHighlightTeamsGoingToKnockout();
                    StandingsActivity.standingsAdapter.notifyDataSetChanged();
                }

                //reset history listview
                if(StandingsActivity.historyAdapter!=null) {
                    StandingsActivity.historyAdapter.clear();
                    StandingsActivity.historyAdapter.notifyDataSetChanged();

                    StandingsActivity.historyAdapter = new MatchHistoryAdapter(getBaseContext(),R.layout.match_history_row, TournamentPoule.matchHistory) ;
                    StandingsActivity.lv_history.setAdapter(StandingsActivity.historyAdapter);
                }

            }
        });

        butToStandings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, StandingsActivity.class);
                startActivity(intent);
            }
        });
    }
}