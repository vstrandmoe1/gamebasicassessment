package osm.gamebasicassesment;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import osm.gamebasicassesment.adapters.MatchHistoryAdapter;
import osm.gamebasicassesment.adapters.PouleTableAdapter;
import osm.gamebasicassesment.utils.AppConfig;
import osm.gamebasicassesment.utils.TournamentPoule;

public class StandingsActivity extends AppCompatActivity {

    public static ArrayAdapter<String> historyAdapter;
    public static ListView lv_tableStanding, lv_history;
    public static PouleTableAdapter standingsAdapter;
    public static ImageView butToHome;
    TextView tvPageTitle, tvHistoryTitle, tvNoMatchResults, tvTableTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_standings);

        lv_tableStanding = (ListView) findViewById(R.id.lv_teams);
        lv_history = (ListView)findViewById(R.id.lv_history);

        butToHome = (ImageView) findViewById(R.id.but_homepage);

        //set fonts
        tvPageTitle = (TextView) findViewById(R.id.tv_page_title);
        AppConfig.setFontToPorky(tvPageTitle, getBaseContext());

        tvHistoryTitle = (TextView) findViewById(R.id.tv_history);
        AppConfig.setFontToPorky(tvHistoryTitle, getBaseContext());

        tvNoMatchResults = (TextView) findViewById(R.id.tv_no_match_results);

        tvTableTitle = (TextView) findViewById(R.id.tv_ranking_table_header);
        AppConfig.setFontToPorky(tvTableTitle, getBaseContext());

        standingsAdapter = new PouleTableAdapter(getBaseContext(), R.layout.current_stats_row, TournamentPoule.rankingTeams);
        lv_tableStanding.setAdapter(standingsAdapter);

        historyAdapter = new MatchHistoryAdapter(getBaseContext(),R.layout.match_history_row, TournamentPoule.matchHistory) ;
        lv_history.setAdapter(historyAdapter);

        butToHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               onBackPressed();
            }
        });
        createTableHeader();

        if(TournamentPoule.matchHistory.size() > 0) {
            tvNoMatchResults.setVisibility(View.GONE);
        }else {
            tvNoMatchResults.setVisibility(View.VISIBLE);

        }
        if(TournamentPoule.matchNumber >= 6) {
            if(standingsAdapter!=null) {
                StandingsActivity.standingsAdapter.highlightTeamsGoingToKnockout();
            }
        }

    }
    @Override
    protected void onResume() {
        super.onResume();

        if(TournamentPoule.matchHistory.size() > 0) {
            tvNoMatchResults.setVisibility(View.GONE);
        }else {
            tvNoMatchResults.setVisibility(View.VISIBLE);

        }
    }

    /**
     * set the ehader titles for the tabl
     */
    private void createTableHeader() {
        LayoutInflater layoutInflater = LayoutInflater.from(getBaseContext());
        View tableheader = layoutInflater.inflate(R.layout.current_stats_row, null);

        TextView position = tableheader.findViewById(R.id.tv_position);
        TextView name = tableheader.findViewById(R.id.tv_name);

        TextView points = tableheader.findViewById(R.id.tv_points);
        TextView wins = tableheader.findViewById(R.id.tv_wins);
        TextView draws = tableheader.findViewById(R.id.tv_draw);
        TextView losses = tableheader.findViewById(R.id.tv_loss);


        //add icon but set invisbile to maintain same format
        ImageView icon = tableheader.findViewById(R.id.iv_flag);
        icon.setImageResource(R.drawable.barca);
        icon.setVisibility(View.INVISIBLE);
        position.setText("Pos");
        name.setText("Team");
        points.setText("Pld");
        wins.setText("W");
        draws.setText("D");
        losses.setText("L");

        lv_tableStanding.addHeaderView(tableheader);
    }
}
