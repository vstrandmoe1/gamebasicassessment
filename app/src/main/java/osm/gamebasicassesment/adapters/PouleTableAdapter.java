package osm.gamebasicassesment.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import osm.gamebasicassesment.R;
import osm.gamebasicassesment.models.Team;
import osm.gamebasicassesment.utils.TournamentPoule;

/**
 * Created by victor on 06/03/2018.
 */

public class PouleTableAdapter extends ArrayAdapter<Team> {

    private int layoutResource;
    public static boolean endOfPoule;

    Context context;

    public PouleTableAdapter(Context context, int layoutResource, ArrayList<Team> tournamentTeams) {
        super(context, layoutResource, tournamentTeams);
        this.layoutResource = layoutResource;
        this.context = context;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;

        if (view == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            view = layoutInflater.inflate(R.layout.current_stats_row, null);
        }

        Team currentTeam = TournamentPoule.rankingTeams.get(position);


        if(currentTeam!=null) {

            TextView team_position = view.findViewById(R.id.tv_position);
            TextView team_name = view.findViewById(R.id.tv_name);
            TextView team_wins = view.findViewById(R.id.tv_wins);
            TextView team_draw = view.findViewById(R.id.tv_draw);
            TextView team_losses = view.findViewById(R.id.tv_loss);
            TextView team_points = view.findViewById(R.id.tv_points);
            TextView team_gf = view.findViewById(R.id.tv_gf);
            TextView team_ga = view.findViewById(R.id.tv_ga);
            TextView team_gd = view.findViewById(R.id.tv_gd);
            ImageView team_icon = view.findViewById(R.id.iv_flag);

            team_name.setText(currentTeam.getStrName());

            team_position.setText(Integer.toString(position + 1));

            team_wins.setText(Integer.toString(currentTeam.getWins()));
            team_draw.setText(Integer.toString(currentTeam.getDraw()));
            team_losses.setText(Integer.toString(currentTeam.getLosses()));
            team_points.setText(Integer.toString(currentTeam.getPoints()));

            team_gf.setText(Integer.toString(currentTeam.getGoalsFor()));
            team_ga.setText(Integer.toString(currentTeam.getGoalsAgainst()));
            team_gd.setText(currentTeam.getGoalsDiff());

            team_icon.setImageResource(currentTeam.getResourceID());

        }

        if(endOfPoule && (position == 0 || position == 1)) {
            view.setBackgroundColor(context.getResources().getColor(R.color.gold));
        }
        return view;
    }

    public void highlightTeamsGoingToKnockout() {
        endOfPoule = true;
    }

    public void removeHighlightTeamsGoingToKnockout() {
        endOfPoule = false;
    }

}
