package osm.gamebasicassesment.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import osm.gamebasicassesment.R;
import osm.gamebasicassesment.models.Team;
import osm.gamebasicassesment.utils.TournamentPoule;

/**
 * Created by victor on 06/03/2018.
 */

public class MatchHistoryAdapter extends ArrayAdapter<String> {

    private int layoutResource;


    public MatchHistoryAdapter(Context context, int layoutResource, ArrayList<String> tournamentTeams) {
        super(context, layoutResource, tournamentTeams);
        this.layoutResource = layoutResource;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;

        if (view == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            view = layoutInflater.inflate(R.layout.match_history_row, null);
        }

        TextView teamNames = view.findViewById(R.id.tv_team_name);
        TextView teamScore = view.findViewById(R.id.tv_team_score);
        ImageView flagHome = view.findViewById(R.id.iv_flagHome);
        ImageView flagAway = view.findViewById(R.id.iv_flagAway);

        if(TournamentPoule.matchHistory!=null && TournamentPoule.matchHistory.size() > 0) {

            String historyTeams = TournamentPoule.matchHistory.get(position);
            String historyScore = TournamentPoule.scoreHistory.get(position);

            //get resource at position +1 because position for standingsAdapter starts at 0 and matchNumber starts at 1.
            int resourceHome = TournamentPoule.iconHistory[position][1];
            int resourceAway = TournamentPoule.iconHistory[position][0];


            if (historyTeams != null) {
                teamNames = view.findViewById(R.id.tv_team_name);
                teamNames.setText(historyTeams);

                teamScore = view.findViewById(R.id.tv_team_score);
                teamScore.setText(historyScore);

                flagHome = view.findViewById(R.id.iv_flagHome);
                flagHome.setImageResource(resourceHome);

                flagAway = view.findViewById(R.id.iv_flagAway);
                flagAway.setImageResource(resourceAway);
            }
        }else {

            teamNames = view.findViewById(R.id.tv_team_name);
            teamNames.setText("");

            teamScore = view.findViewById(R.id.tv_team_score);
            teamScore.setText("");

            flagHome = view.findViewById(R.id.iv_flagHome);
            flagHome.setImageResource(0);

            flagAway = view.findViewById(R.id.iv_flagAway);
            flagAway.setImageResource(0);
        }
        return view;
    }

}
