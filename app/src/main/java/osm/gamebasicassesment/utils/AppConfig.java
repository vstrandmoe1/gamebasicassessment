package osm.gamebasicassesment.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

/**
 * Created by victor on 10/03/2018.
 */

public class AppConfig {


    public static void setFontToPorky(TextView view, Context context) {
        Typeface custom_font = Typeface.createFromAsset(context.getAssets(),  "fonts/PORKYS_.TTF");
        view.setTypeface(custom_font);
    }
}
