package osm.gamebasicassesment.utils;

import java.util.Comparator;

import osm.gamebasicassesment.models.Team;

/**
 * Created by victor on 08/03/2018.
 */

public class TeamComperator implements Comparator<Team>
{
    public int compare(Team left, Team right) {
        return left.compareTo(right);
    }
}