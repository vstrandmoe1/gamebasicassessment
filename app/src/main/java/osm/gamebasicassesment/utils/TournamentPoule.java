package osm.gamebasicassesment.utils;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;

import osm.gamebasicassesment.R;
import osm.gamebasicassesment.models.Team;

/**
 * Created by victor on 07/03/2018.
 */

public class TournamentPoule {

    public static ArrayList<Team> rankingTeams;
    public static ArrayList<Team> matchSchedule;


    public static ArrayList<String> matchHistory;
    public static ArrayList<String> scoreHistory;
    public static int[][] iconHistory;

    public static int matchNumber;


    public static void createNewTournament() {

        //two seperate arrays. 1 that remains unchanged and contains the match schedule
        // the other to keep track of points
        matchSchedule = new ArrayList<>();
        rankingTeams = new ArrayList<>();

        //List to track match history
        matchHistory = new ArrayList<>();
        matchHistory.clear();

        //ist to track score history
        scoreHistory = new ArrayList<>();
        scoreHistory.clear();


        //here will be saved the resource id's of the flag combintions
        iconHistory = new int [6][2];

        //create static team objects to keep the app conecpt simple
        Team team1 = new Team("Juventus", 7.8, 6.7, 7.0 ,1, R.drawable.juventus);
        Team team2 = new Team("Barcelona", 9.1, 8.8, 8.9 ,2, R.drawable.barca);
        Team team3 = new Team("Monaco", 2.8, 3.4, 4.8 ,3, R.drawable.monaco);
        Team team4 = new Team("Real Madrid", 9.0, 9.1, 8.4 ,4, R.drawable.barca);

        matchSchedule.add(team1);
        matchSchedule.add(team2);
        matchSchedule.add(team3);
        matchSchedule.add(team4);

        rankingTeams.add(team1);
        rankingTeams.add(team2);
        rankingTeams.add(team3);
        rankingTeams.add(team4);

        //reset match number
        matchNumber = 1;
    }

    /**
     * Logid to play correct game in the schedule
     */
    public static void playNextGame() {
       //extra added safety code to prevent crash
        if(matchNumber > 6) {
            matchNumber = 1;
        }

        int results;

        int pHome = -1;
        int pAway = -1;
        //Find the correct team in the Arraylist according to the schedule
        switch(matchNumber) {
            case 1:
                // team 1 - team 2
                for(int i = 0; i < rankingTeams.size(); i++) {
                    if(rankingTeams.get(i).scheduleID == 1)
                    {
                        pHome = i;
                    }
                    if(rankingTeams.get(i).scheduleID == 2)
                    {
                        pAway = i;
                    }
                }

                results = GameEngine.playMatch(rankingTeams.get(pHome), rankingTeams.get(pAway));

                //adjust team points accordingly
                //set goal difference
                rankingTeams.get(pHome).increaseGoalsForBy(GameEngine.homeScore);
                rankingTeams.get(pHome).increaseGoalsAgainstBy(GameEngine.awayScore);

                rankingTeams.get(pHome).getCalculatedGoalsDiff();

                rankingTeams.get(pAway).increaseGoalsForBy(GameEngine.awayScore);
                rankingTeams.get(pAway).increaseGoalsAgainstBy(GameEngine.homeScore);

                if(results == 1) {
                    rankingTeams.get(pHome).addWin();
                    rankingTeams.get(pAway).addLoss();
                }else if(results == 0) {
                    rankingTeams.get(pHome).addDraw();
                    rankingTeams.get(pAway).addDraw();
                }
                else {
                    rankingTeams.get(pAway).addWin();
                    rankingTeams.get(pHome).addLoss();
                }
            break;
            case 2:
                // team 3 - team 4
                for(int i = 0; i < rankingTeams.size(); i++) {
                    if(rankingTeams.get(i).scheduleID == 3)
                    {
                        pHome = i;
                    }
                    if(rankingTeams.get(i).scheduleID == 4)
                    {
                        pAway = i;
                    }
                }
                results = GameEngine.playMatch(rankingTeams.get(pHome), rankingTeams.get(pAway));
                //adjust team points accordingly

                rankingTeams.get(pHome).increaseGoalsForBy(GameEngine.homeScore);
                rankingTeams.get(pHome).increaseGoalsAgainstBy(GameEngine.awayScore);

                rankingTeams.get(pHome).getCalculatedGoalsDiff();

                rankingTeams.get(pAway).increaseGoalsForBy(GameEngine.awayScore);
                rankingTeams.get(pAway).increaseGoalsAgainstBy(GameEngine.homeScore);

                if(results == 1) {
                    rankingTeams.get(pHome).addWin();
                    rankingTeams.get(pAway).addLoss();


                }else if(results == 0) {
                    rankingTeams.get(pAway).addDraw();
                    rankingTeams.get(pHome).addDraw();
                }
                else {
                    rankingTeams.get(pAway).addWin();
                    rankingTeams.get(pHome).addLoss();
                }
            break;
            case 3:
                // team 4 - team 1
                for(int i = 0; i < rankingTeams.size(); i++) {
                    if(rankingTeams.get(i).scheduleID == 4)
                    {
                        pHome = i;
                    }
                    if(rankingTeams.get(i).scheduleID == 1)
                    {
                        pAway = i;
                    }
                }
            results = GameEngine.playMatch(rankingTeams.get(pHome), rankingTeams.get(pAway));
                //adjust team points accordingly
                rankingTeams.get(pHome).increaseGoalsForBy(GameEngine.homeScore);
                rankingTeams.get(pHome).increaseGoalsAgainstBy(GameEngine.awayScore);

                rankingTeams.get(pHome).getCalculatedGoalsDiff();

                rankingTeams.get(pAway).increaseGoalsForBy(GameEngine.awayScore);
                rankingTeams.get(pAway).increaseGoalsAgainstBy(GameEngine.homeScore);

                if(results == 1) {
                    rankingTeams.get(pHome).addWin();
                    rankingTeams.get(pAway).addLoss();
                }else if(results == 0) {
                    rankingTeams.get(pHome).addDraw();
                    rankingTeams.get(pAway).addDraw();
                }
                else {
                    rankingTeams.get(pAway).addWin();
                    rankingTeams.get(pHome).addLoss();
                }
            break;
            case 4:
                // team 2 - team 3
                for(int i = 0; i < rankingTeams.size(); i++) {
                    if(rankingTeams.get(i).scheduleID == 2)
                    {
                        pHome = i;
                    }
                    if(rankingTeams.get(i).scheduleID == 3)
                    {
                        pAway = i;
                    }
                }
                results = GameEngine.playMatch(rankingTeams.get(pHome), rankingTeams.get(pAway));
                //adjust team points accordingly
                rankingTeams.get(pHome).increaseGoalsForBy(GameEngine.homeScore);
                rankingTeams.get(pHome).increaseGoalsAgainstBy(GameEngine.awayScore);

                rankingTeams.get(pHome).getCalculatedGoalsDiff();

                rankingTeams.get(pAway).increaseGoalsForBy(GameEngine.awayScore);
                rankingTeams.get(pAway).increaseGoalsAgainstBy(GameEngine.homeScore);

                if(results == 1) {
                    rankingTeams.get(pHome).addWin();
                    rankingTeams.get(pAway).addLoss();
                }else if(results == 0) {
                    rankingTeams.get(pHome).addDraw();
                    rankingTeams.get(pAway).addDraw();
                }
                else {
                    rankingTeams.get(pAway).addWin();
                    rankingTeams.get(pHome).addLoss();
                }
                break;
            case 5:
                // team 4 - team 2
                for(int i = 0; i < rankingTeams.size(); i++) {
                    if(rankingTeams.get(i).scheduleID == 4)
                    {
                        pHome = i;
                    }
                    if(rankingTeams.get(i).scheduleID == 2)
                    {
                        pAway = i;
                    }
                }
                results = GameEngine.playMatch(rankingTeams.get(pHome), rankingTeams.get(pAway));
                //adjust team points accordingly
                rankingTeams.get(pHome).increaseGoalsForBy(GameEngine.homeScore);
                rankingTeams.get(pHome).increaseGoalsAgainstBy(GameEngine.awayScore);

                rankingTeams.get(pHome).getCalculatedGoalsDiff();

                rankingTeams.get(pAway).increaseGoalsForBy(GameEngine.awayScore);
                rankingTeams.get(pAway).increaseGoalsAgainstBy(GameEngine.homeScore);

                if(results == 1) {
                    rankingTeams.get(pHome).addWin();
                    rankingTeams.get(pAway).addLoss();
                }else if(results == 0) {
                    rankingTeams.get(pHome).addDraw();
                    rankingTeams.get(pAway).addDraw();
                }
                else {
                    rankingTeams.get(pAway).addWin();
                    rankingTeams.get(pHome).addLoss();
                }
                break;

            case 6:
                // team 1 - team 3
                for(int i = 0; i < rankingTeams.size(); i++) {
                    if(rankingTeams.get(i).scheduleID == 1)
                    {
                        pHome = i;
                    }
                    if(rankingTeams.get(i).scheduleID == 3)
                    {
                        pAway = i;
                    }
                }
                results = GameEngine.playMatch(rankingTeams.get(pHome), rankingTeams.get(pAway));
                //adjust team points accordingly
                rankingTeams.get(pHome).increaseGoalsForBy(GameEngine.homeScore);
                rankingTeams.get(pHome).increaseGoalsAgainstBy(GameEngine.awayScore);

                rankingTeams.get(pHome).getCalculatedGoalsDiff();

                rankingTeams.get(pAway).increaseGoalsForBy(GameEngine.awayScore);
                rankingTeams.get(pAway).increaseGoalsAgainstBy(GameEngine.homeScore);

                if(results == 1) {
                    rankingTeams.get(pHome).addWin();
                    rankingTeams.get(pAway).addLoss();
                }else if(results == 0) {
                    rankingTeams.get(pHome).addDraw();
                    rankingTeams.get(pAway).addDraw();
                }
                else {
                    rankingTeams.get(pAway).addWin();
                    rankingTeams.get(pHome).addLoss();
                }

                break;
        }

        matchHistory.add(rankingTeams.get(pHome).getStrName() + " - " + rankingTeams.get(pAway).getStrName());
        scoreHistory.add(GameEngine.getCurrentMatchResultsInString());

        //used multidimensional array for this one to show I'm not only familiar with ArrayList library
        iconHistory[matchNumber-1][0] = rankingTeams.get(pHome).getResourceID();
        iconHistory[matchNumber-1][1] = rankingTeams.get(pAway).getResourceID();

        matchNumber++;

        adjustTableStandings();
    }

    private static void adjustTableStandings() {
        //sort the table again according to which team has the most points
        Collections.sort(rankingTeams, new TeamComperator());

    }
}
