package osm.gamebasicassesment.utils;

import android.os.CountDownTimer;
import android.util.Log;

import java.util.Random;

import osm.gamebasicassesment.models.Team;

/**
 * Created by victor on 06/03/2018.
 */

public class GameEngine {

    public static String matchResult;

    public static int homeScore = 0 ;
    public static int awayScore = 0;

    //This integer 1 or 0 will be given to the home team or away team if they have advantage, respectively
    private static int gameAdvantage;

    /**
     * Runs the simulation that plays a game between 2 teams.
     * Returns: 1 if teamHome wins, -1 if teamAway wins or 0 for sameScore
     * @param teamHome
     * @param teamAway
     * @return
     */
    public static int playMatch(Team teamHome, Team teamAway) {

        homeScore = 0;
        awayScore = 0;

        //how many iterrations the simulation will run is 10
        //average at 1 attackignmoves in 10 minutes
        for (int turnsLeft = 10; turnsLeft > 0; turnsLeft--) {
            if (turnsLeft == 10) {
                gameAdvantage = 1;
            }
                switch (gameAdvantage) {
                    case 1:
                        homeAttack(teamAway, teamHome);
                        break;
                    case 0:
                        awayAttack(teamHome, teamAway);
                        break;
                }
            }
            //after the match is played, based on the end score, return which team wins
            if (homeScore > awayScore) {
                return 1;
            } else if (homeScore == awayScore) {
                return 0;
            } else {
                return -1;
            }
    }

    public static String getCurrentMatchResultsInString() {
        String matchResult = Integer.toString(homeScore) + " - " + Integer.toString(awayScore);
        return matchResult;
    }

        /**
         * Performs an attack form the hoem team upon enemy team.
         * Calculates through the Team's attak power versus the opponent's defense
         */
    private static void homeAttack(Team teamAway, Team teamHome) {

        //first up is attack of the home Team. They have advantage.
        double homeAttackPower = teamHome.getdAttack() * roleDie();
        double awayAttackPower = teamAway.getdAttack() * roleDie();

        //if home team is stronger, move onto midfield
        if(homeAttackPower >= awayAttackPower) {
            gameAdvantage = 1;
            //away team midfield power
            double awayMidPower = teamAway.getdAttack() * roleDie();
            if(homeAttackPower >= awayMidPower) {
                //away team midfield power
                double awayDefPower = teamAway.getdAttack() * roleDie();
                if(homeAttackPower >=awayDefPower) {
                    //GOAL!
                    homeScore++;
                    gameAdvantage = 0;
                }

            }else {
                gameAdvantage = 0;
            }


        }else {
            gameAdvantage = 0;
        }
}
    private static void awayAttack(Team teamHome, Team teamAway) {

        //first up is attack of the home Team. They have advantage.
        double homeAttackPower = teamHome.getdAttack() * roleDie();
        double awayAttackPower = teamAway.getdAttack() * roleDie();
        //if away team is stronger, move onto midfield
        if(awayAttackPower >= homeAttackPower) {
            gameAdvantage = 0;
            //home team midfield power
            double homeMidPower = teamHome.getdAttack() * roleDie();
            if(awayAttackPower >= homeMidPower) {
                //away team midfield power
                double homeDefPower = teamHome.getdAttack() * roleDie();
                if(awayAttackPower >=homeDefPower) {
                    //GOAL!
                    awayScore++;
                    gameAdvantage = 1;
                }
            }else {
                gameAdvantage = 1;
            }
        }else {
            // ball is taken from opponent. advantage changes
            gameAdvantage = 1;

        }
    }

    private static int roleDie() {
        Random rn = new Random();
        int answer = rn.nextInt(10 - 1) + 1;

        return answer;
    }

    public static String getMatchResult() {
        return  matchResult;
    }
}
