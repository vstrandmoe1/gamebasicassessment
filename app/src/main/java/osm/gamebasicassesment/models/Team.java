package osm.gamebasicassesment.models;

import android.util.Log;

/**
 *
 * Basic model class for a single Team object
 * Contains basic properties of a team, name , attack, deffense, middle power
 */

public class Team implements Comparable<Team>{

    private String strName;
    private double  dAttack;
    private double dDef;
    private double dMid;

    private int wins;
    private int draw;
    private int losses;
    private int goalsFor;
    private int goalsAgainst;
    private String goalsDiff;

    private int points;

    public int scheduleID;
    private int resourceID;

    @Override
    public int compareTo(Team team1) {
        if(team1.getPoints() < this.getPoints()){
            return -1;
        }
        else if(team1.getPoints() == this.getPoints()) {
            //Here comes the logic of Alphabetical comparing

            return 1;
        }
        else {
            return 1;
        }
    }

    public int getResourceID() {
        return resourceID;
    }
    public Team(String name, double attack, double mid, double def, int scheduleID, int resourceID) {
        this.strName = name;
        this.dAttack = attack;
        this.dDef = def;
        this.dMid = mid;
        this.scheduleID = scheduleID;
        this.resourceID = resourceID;
        wins = 0;
        losses = 0;
        draw = 0;
        points  = 0;
        goalsFor = 0;
        goalsAgainst = 0;

    }

    public int getGoalsFor() {
        return goalsFor;
    }

    public void setGoalsFor(int goalsFor) {
        this.goalsFor = goalsFor;
    }

    public int getGoalsAgainst() {
        return goalsAgainst;
    }

    public void setGoalsAgainst(int goalsAgainst) {
        this.goalsAgainst = goalsAgainst;
    }


    public void increaseGoalsForBy(int goals) {
        this.goalsFor+= goals;
        Log.d("GD_GOALS", " name: " + strName);

        Log.d("GD_GOALS", " goals for: " + goalsFor);
    }
    public void increaseGoalsAgainstBy(int goals) {
        this.goalsAgainst+= goals;
    }
    public int getCalculatedGoalsDiff() {
        return this.goalsFor - this.goalsAgainst;
    }

    public String getGoalsDiff() {
        if( this.goalsFor - this.goalsAgainst > 0) {
            goalsDiff = "+" + Integer.toString(this.goalsFor - this.goalsAgainst);
        }else if(this.goalsFor - this.goalsAgainst < 0) {
            goalsDiff = Integer.toString(this.goalsFor - this.goalsAgainst);
        }else {
            goalsDiff = "0";
        }
        return goalsDiff;
    }

    public int getPoints() {
        return points;
    }

    /**
     * 3 points for winning team
     */
    public void addWin() {
        wins++;
        points +=3;
    }

    /**
     * 0 points for losing team
     */
    public void addLoss() {
        losses++;
    }

    /**
     * 1 point for draw
     */
    public void addDraw() {
        draw++;
        points++;
    }
    public String getStrName() {
        return strName;
    }


    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }

    public int getLosses() {
        return losses;
    }

    public void setLosses(int losses) {
        this.losses = losses;
    }
    public void setStrName(String strName) {
        this.strName = strName;
    }

    public double getdAttack() {
        return dAttack;
    }

    public void setdAttack(double dAttack) {
        this.dAttack = dAttack;
    }

    public double getdDef() {
        return dDef;
    }

    public void setdDef(double dDef) {
        this.dDef = dDef;
    }

    public double getdMid() {
        return dMid;
    }

    public void setdMid(double dMid) {
        this.dMid = dMid;
    }


}
